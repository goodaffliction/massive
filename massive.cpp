﻿#include <iostream>
#include <sstream>
#include <string>

int main()
{
    std::string date = __DATE__;
    std::cout << date<<"\n";
    int day;
    std::string chislo;
    chislo.push_back(date[4]);
    chislo.push_back(date[5]);
    std::stringstream ss;

    ss << chislo;
    ss >> day;
    std::cout << day<<"\n";

    const int N = 10;
    int sum=0;
    int array[N][N] = {};

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            //Vyvod massiva
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
            //Summa elementov stroki
            if (day% N-1 == i)
            {
                sum += array[i][j];
            }
        }
        std::cout << "\n";
    }  
    std::cout << "Summa=" << sum << "\n";
    return 0;
}


